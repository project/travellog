<?php 
/**
* Generate HTML for all travel logs of the current user
* @returns HTML page
*/
function travellog_main() {
  //declare global variables 
  global $user;
  global $base_url;

  //initialize variable 
  $output = '';

  //check if there are any vehicles 
  //for current user 
  $sql  = "select count(*) as row_count ";
  $sql .= "from travellog_vehicles ";
  $sql .= "where user_key = %d ";
  
  //query the database 
  $result = db_query($sql, $user->uid);
  //get the results 
  $row = db_fetch_object($result);
  //initialize the variable   
  $flag_has_vehicles = FALSE;
  //check the results 
  if ($row->row_count > 0) {
    $flag_has_vehicles = TRUE;
  }

  //check if there are any travel log types 
  //set up for current user 
  $sql  = "select count(*) as row_count ";
  $sql .= "from travellog_types ";
  $sql .= "where user_key = %d or default_flag = 1 "; 
  
  //query the database 
  $result = db_query($sql, $user->uid);
  //get the results
  $row = db_fetch_object($result);
  //initialize the variable 
  $flag_has_types = FALSE;
  //check the results  
  if ($row->row_count > 0) {
     $flag_has_types = TRUE;
  }
   
  //if there are no vehicles
  if (!$flag_has_vehicles) {
  //show the link to add new vehicles
    $output .= "<p>Before you can start using Travel Logs, you will need to set up some vehicles.  </p>";
    $output .= "<p>Set up a bicycle for a cyclist log, a car for a road trip log, and a jet for a air / international travel log</p>";
    $output .= "<p>Click the link below.... </p>";
    $output .= "<p><a href=\"" . $base_url . "/travellog/vehicles/add\">Add Rides</a></p>";
  }
  //if there are no travel log types
  else if (!$flag_has_types) {
    //show the link to add travel log types
    $output .= "<p>Before you can start using Travel Logs, you will need to set up some log types.  </p>";
    $output .= "<p>Examples of log types are cyclist logs, road trip logs, air / international travel logs</p>";
    $output .= "<p>Click the link below.... </p>";
    $output .= "<p><a href=\"" . $base_url . "/travellog/types/add\">Add Types</a></p>";
  }
  //nothing left to check show the listing 
  else {
    $output .= "<p>Your current travel logs:</p>";
    //start of the table 
	 $output .= "<table>";
	 //table header  
	 $output .= "<tr>";
	 $output .= "<th>Name</th>";
	 $output .= "<th>Description</th>";
	 $output .= "<th>Miles</th>";
	 $output .= "<th>&nbsp;</th>";
	 $output .= "<th>&nbsp;</th>";
	 $output .= "<th>&nbsp;</th>";
	 $output .= "</tr>";

    //set up the query to get all travel logs
    //for current user 
    $sql = "select l.`key`, l.`name`, l.description, sum(est_miles) total_est_miles ";
    $sql .="from travellog l left join travellog_entries e ";
    $sql .="on l.key = e.travellog_key ";
    $sql .="where l.user_key = %d ";
    $sql .="group by l.`key`, l.`name`, l.description ";
    
    //query the database 
    $result = db_query($sql, $user->uid);
    
    //get the results and build each table row
    //for each record returned   
    while ($row = db_fetch_object($result)) {
      $output .= "<tr>";
      $output .= "<td>".t($row->name)."</td>";
      $output .= "<td>".t($row->description)."</td>";
      $output .= "<td>".t($row->total_est_miles)."</td>";
      $output .= "<td><a href=\"" . $base_url . "/travellog/logs/" . $row->key . "\">Manage Log Entries</a></td>";
      $output .= "<td><a href=\"" . $base_url . "/travellog/edit/" . $row->key . "\">Edit</a></td>";
      $output .= "<td><a href=\"" . $base_url . "/travellog/delete/" . $row->key . "\">Delete</a></td>";
      $output .= "</tr>";
    }
    
	 
	 //end the table 
	 $output .= "</table>";
	 
	 //add links to manage other records
	 //for travel logs
    $output .= "<a href=\"" . $base_url . "/travellog/add\">>>Add Log</a>&nbsp;&nbsp;";
    $output .= "<a href=\"" . $base_url . "/travellog/types\">>>Manage Log Types</a>&nbsp;&nbsp;"; 
    $output .= "<a href=\"" . $base_url . "/travellog/vehicles\">>>Manage Rides</a>";
  }
  
  $output .= "<p>&nbsp;</p>";
  $output .= travellog_chart_user_daily($user->uid);
  
  return $output;
} //function travellog_main 

/**
* Display the travel log add new, edit, and delete forms
* @param form_state by reference array of all post values of the form
* @param trans_type string that tells what type of transaction is being performed - add, edit, delete
* @param travellog_key integer that identifies the current record to edit or delete. null or 0 when adding a record 
* @return array form 
*/
function travellog_form(&$form_state, $trans_type, $travellog_key) {
  global $user;
  $form = array();

  if ($trans_type == 'add') {
    $form['title'] = array(
      '#value' => t('<p>Add New Travel Log</p>'),
    );
    $form_state['current_form'] = 'travellog_add';
	 $form['name'] = array(
	   '#type' => 'textfield',
	   '#title' => t('Name'),
      '#size' => 16, 
	 );
	                 
	 $form['description'] = array(
	   '#type' => 'textfield',
	   '#title' => t('Description'),
	 );

    $options[] = t('<select ride>');
    $sql = 'select `key`, name from travellog_vehicles where user_key = %d order by 2';
    $result = db_query ($sql, $user->uid );
	 while ($row = db_fetch_object($result)) {
	   $options[$row->key] = t($row->name);
    }
	 $form['travellog_vehicles_key'] = array(
	   '#type' => 'select',
	   '#title' => t('Vehicle'),
	   '#default_value' => 0,
	   '#options' => $options,
	 );

    unset ($options);
    $options[] = t('<select type>');
    $sql = 'select `key`, name from travellog_types where user_key = %d order by 2';
    $result = db_query ($sql, $user->uid );
	 while ($row = db_fetch_object($result)) {
	   $options[$row->key] = t($row->name);
    }
	 $form['travellog_types_key'] = array(
	   '#type' => 'select',
	   '#title' => t('Log Type'),
	   '#default_value' => 0,
	   '#options' => $options, 
	 );
	  
	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Save'),
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 );
  } //add new travel log * @param form_state by reference array of all post values of the form
  else if ($trans_type == 'delete') {
    $form_state['current_form'] = 'travellog_delete';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_key );
    
    $sql  = "select `name`, description, travellog_vehicles_key, travellog_types_key ";
    $sql .= "from travellog ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_key );
    $travellog_row = db_fetch_object($result);
    
    $form['title'] = array(
      '#value' => t('<p>Delete ' . t($travellog_row->name) . ' Travel Log</p>'),
    );

	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Delete'),
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 );
  } //delete 
  else if ($trans_type == 'edit') {
    $form_state['current_form'] = 'travellog_edit';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_key );
    
    $sql  = "select `name`, description, travellog_vehicles_key, travellog_types_key ";
    $sql .= "from travellog ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_key );
    $travellog_row = db_fetch_object($result);
    
    $form['title'] = array(
      '#value' => t('<p>Edit ' . t($travellog_row->name) . ' Travel Log</p>'),
    );

	 $form['name'] = array(
	   '#type' => 'textfield',
	   '#title' => t('Name'),
     '#size' => 16,
     '#default_value' => t($travellog_row->name), 
	 );
	                 
	 $form['description'] = array(
	   '#type' => 'textfield',
	   '#title' => t('Description'),
	   '#default_value' => $travellog_row->description, 
	 );

    $options[] = t('<select ride>');
    $sql = 'select `key`, name from travellog_vehicles where user_key = %d order by 2';
    $result = db_query ($sql, $user->uid);
	 while ($row = db_fetch_object($result)) {
	   $options[$row->key] = $row->name;
    }
	 $form['travellog_vehicles_key'] = array(
	   '#type' => 'select',
	   '#title' => t('Vehicle'),
	   '#default_value' => $travellog_row->travellog_vehicles_key,
	   '#options' => $options, 
	 );

    unset ($options);
    $options[] = t('<select type>');
    $sql = 'select `key`, name from travellog_types where user_key = %d order by 2';
    $result = db_query ($sql, $user->uid);
	 while ($row = db_fetch_object($result)) {
	   $options[$row->key] = t($row->name);
    }
   $form['travellog_types_key'] = array(
	   '#type' => 'select',
	   '#title' => t('Log Type'),
	   '#default_value' => $travellog_row->travellog_types_key,
	   '#options' => $options, 
	 );

	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Save'),
	 );
    $form['delete'] = array (
      '#type' => 'button',
	   '#value' => t('Delete'),
	   '#executes_submit_callback' => TRUE,
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 ); 
  } //edit 
  else {
    $form_state['current_form'] = 'travellog_main';
    $form['log_title'] = array(
      '#value' => t(travellog_main()),
    );
  } //anything else 
    
  return $form;
} //travellog_form

/**
* Processes the form validate hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_form_validate($form, &$form_state){
  //don't validate if it's a cancel or delete  
  if ($form_state['values']['op'] == 'Save'){
    $name = trim(check_plain($form_state['values']['name']));
    if ($name =='') {
      form_set_error('Validation', t('Please name your log'));
    }
    if ($form_state['values']['travellog_vehicles_key'] == 0) {
      form_set_error('Validation', t('Please select a ride'));
    }
    if ($form_state['values']['travellog_types_key'] == 0) {
      form_set_error('Validation', t('Please select a travel log type'));
    }
  }
}
/**
* Processes the form submit hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_form_submit($form, &$form_state){
  global $user;
  global $base_url; 
  if ($form_state['values']['op'] == 'Save') {
    if ($form_state['current_form'] == 'travellog_add') {
	    try {
	      $sql = "insert into travellog "; 
	      $sql.= "(user_key, name, description, travellog_types_key, travellog_vehicles_key) ";
	      $sql.= "values ";
	      $sql.="(%d,'%s','%s', %d,%d) ";
	      
	      if (!db_query ($sql
	                   , $user->uid 
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['description'])
	                   , $form_state['values']['travellog_types_key']
	                   , $form_state['values']['travellog_vehicles_key'])
	                   ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog"; 
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //Add
    else if ($form_state['current_form'] == 'travellog_edit') {
	    try {
	      $sql = "update travellog "; 
	      $sql.= "set ";
	      $sql.= "name = '%s', ";
	      $sql.= "description = '%s', ";
	      $sql.= "travellog_types_key = %d, ";
	      $sql.= "travellog_vehicles_key = %d ";
         $sql.= "where `key` = %d ";
	      
	      if (!db_query ($sql
                     , trim($form_state['values']['name'])
	                   , trim($form_state['values']['description'])
	                   , $form_state['values']['travellog_types_key']
	                   , $form_state['values']['travellog_vehicles_key']
	                   , $form_state['values']['key']
	                   )
	          ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //travellog_edit 
  } //Save
  else if ($form_state['values']['op'] == 'Delete') {
    if ($form_state['current_form'] == 'travellog_edit') {
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = $base_url . "/travellog/delete/".$form_state['values']['key'];
    } //travellog_vehicles_edit
    else {
	    try {
	      $sql = "delete from travellog "; 
	      $sql.= "where `key` = %d ";
	      if (!db_query ($sql,$form_state['values']['key'])){
	        throw new Exception('Could not delete record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been deleted.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/" . $form_state['values']['travellog_key'];
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
	  } //travellog_delete 
  }//delete
  //everything else 
  else if ($form_state['values']['op'] == 'Cancel') {
    drupal_set_message(t('Cancelled.'),'status');
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = $base_url . "/travellog";
  } //cancel
} //travellog_form_submit
 
?>