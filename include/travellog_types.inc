<?php

/**
* Generate HTML for all travel log types of the current user
* @returns HTML page
*/
function travellog_types_main () {
  global $user;
  $output = '';
  $output .= "<table>";
  $output .= "<tr>";
  $output .= "<th>Name</th>";
  $output .= "<th>Description</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "</tr>";

  $sql  = "select `key`, `name`, description ";
  $sql .= "from travellog_types ";
  $sql .= "where default_flag = 1 ";
  $sql .= "order by `name` ";
  
  $result = db_query($sql, $user->uid);
  
  while ($row = db_fetch_object($result)) {
    $output .= "<tr>";
    $output .= "<td>".$row->name."</td>";
    $output .= "<td>".$row->description ."</td>";
    $output .= "<td>&nbsp;</td>";
    $output .= "<td>&nbsp;</td>";
    $output .= "</tr>";
  }

  $sql  = "select `key`, `name`, description ";
  $sql .= "from travellog_types ";
  $sql .= "where user_key = %d ";
  $sql .= "order by `name` ";
  
  $result = db_query($sql, $user->uid);
  
  while ($row = db_fetch_object($result)) {
    $output .= "<tr>";
    $output .= "<td>".$row->name."</td>";
    $output .= "<td>".$row->description ."</td>";
    $output .= "<td><a href=\"" . $base_url . "/travellog/types/edit/" . $row->key . "\">Edit</a></td>";
    $output .= "<td><a href=\"" . $base_url . "/travellog/types/delete/" . $row->key . "\">Delete</a></td>";
    $output .= "</tr>";
  }

  $output .= "</table>";
  
  $output .= "<a href=\"" . $base_url . "/travellog/types/add\">>>Add Type</a>&nbsp;&nbsp;";
  $output .= "<p></p>";
  return $output;
} //function travellog_types_form 

/**
* Display the travel log types add new, edit, and delete forms
* @param form_state by reference array of all post values of the form
* @param trans_type string that tells what type of transaction is being performed - add, edit, delete
* @param travellog_types_key integer that identifies the current record to edit or delete. null or 0 when adding a record 
* @return array form 
*/
function travellog_types_form (&$form_state, $trans_type, $travellog_types_key){

  if ($trans_type == 'add') {
    $form_state['current_form'] = 'travellog_types_add';
    
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
    );
    $form['submit'] = array (
       '#type' => 'submit',
       '#value' => t('Save'),
    );
    $form['cancel'] = array (
       '#type' => 'button',
       '#value' => t('Cancel'),
       '#executes_submit_callback' => TRUE,
    );

  } //add
  else if ($trans_type == 'edit') {
    $form_state['current_form'] = 'travellog_types_edit';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_types_key );
    
    $sql  = "select `name`, description ";
    $sql .= "from travellog_types ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_types_key );
    $row = db_fetch_object($result);
    
    $form['title'] = array(
      '#value' => t('<p>Edit ' . t($row->name) . " Log Type</p>" ),
    );

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => t($row->name),      
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => t($row->description),
    );

	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Save'),
	 );
    $form['delete'] = array (
      '#type' => 'button',
	   '#value' => t('Delete'),
	   '#executes_submit_callback' => TRUE,
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 ); 
  } //edit 
  else if ($trans_type == 'delete') {
    $form_state['current_form'] = 'travellog_types_delete';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_types_key );
    
    $sql  = "select `name`, description ";
    $sql .= "from travellog_types ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_types_key );
    $row = db_fetch_object($result);
       
    $form['title'] = array(
      '#value' => t('<p>Delete ' . t($row->name) . " Log Type</p>" ),
    );
	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Delete'),
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 );
  } //delete 
  else {
    $form_state['current_form'] = 'travellog_types_main';
    $form['title'] = array(
      '#value' => t(travellog_types_main()),
    );
  } //anything else
  return $form;
} //function travellog_types_form 

/**
* Processes the form validate hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_types_form_validate($form, &$form_state) {
  //don't validate if it's a cancel
  if ($form_state['values']['op'] == 'Save'){
    $name = trim(check_plain($form_state['values']['name']));
    if ($name=='') {
      form_set_error('Validation', t('Please enter a name'));
    }
  }
} //function travellog_types_form_validate 
/**
* Processes the form submit hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_types_form_submit ($form, &$form_state) {
  global $user;
  global $base_url;

  if ($form_state['values']['op'] == 'Save') {
    if ($form_state['current_form'] == 'travellog_types_add') {
	    try {
	      $sql = "insert into travellog_types "; 
	      $sql.= "(user_key, name, description) ";
	      $sql.= "values ";
	      $sql.="(%d,'%s','%s') ";
	      
	      if (!db_query ($sql
	                   , $user->uid 
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['description'])
	                   )
	          ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
        $form_state['redirect'] = $base_url . "/travellog/types";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //Add
    else if ($form_state['current_form'] == 'travellog_types_edit') {
	    try {
	      $sql = "update travellog_types "; 
	      $sql.= "set ";
	      $sql.= "name = '%s', ";
	      $sql.= "description = '%s' ";
        $sql.= "where `key` = %d ";
	      
	      if (!db_query ($sql
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['description'])
	                   , $form_state['values']['key']
	                   )
	          ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/types";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //edit 
  } //Save
  else if ($form_state['values']['op'] == 'Delete') {
    if ($form_state['current_form'] == 'travellog_types_edit') {
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = $base_url . "/travellog/types/delete/".$form_state['values']['key'];
    } //travellog_types_delete 
    else {
	    try {
	      $sql = "delete from travellog_types "; 
	      $sql.= "where `key` = %d ";
	      	      
	      if (!db_query ($sql
	                   , $form_state['values']['key']
	                   )
	          ) {
	        throw new Exception('Could not delete record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been deleted.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/types";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
	  } //travellog_types_delete 
  }//Delete
  else if ($form_state['values']['op'] == 'Cancel') {
    drupal_set_message(t('Cancelled.'),'status');
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = $base_url . "/travellog/types";
  } //cancel
} //function travellog_types_form_submit
?>