<?php

function travellog_chart_group_summary($travellog_types_key) {

  if (!module_exists('open_flash_chart_api')) {
    return ' ';
  }
  global $user;

  $output ="";
  
  //check to see if there are any people mapped for selected vehicle type
  $sql = "select ifnull(count(*),0) count_users ";
  $sql .= "from travellog t join travellog_group_map tgm "; 
  $sql .= "on t.user_key = tgm.user_key ";
  $sql .= "where t.travellog_types_key = %d ";
 
  //query the database 
  $result = db_query($sql,$travellog_types_key);
  $row = db_fetch_object($result);
  if ($row->count_users  <= 0) {
    $output = "<p>No participants currently enrolled</p>";
    return $output;
  }
  
  $sql = "select `name` from travellog_types where `key` = %d ";
  //query the database 
  $result = db_query($sql,$travellog_types_key);
  $row = db_fetch_object($result);
  $output .= "<h2>".t($row->name)." Summary</h2>";
  $sql = "SELECT tg.`name` group_name, ifnull(count( distinct tgm.user_key),0) count_users , ifnull(sum(te.est_miles),0) total_est_miles "; 
  $sql.= "FROM ((`travellog_groups` tg left join `travellog_group_map` tgm  "; 
  $sql.= "on tg.key = tgm.travellog_groups_key) left join travellog t  "; 
  $sql.= "on tgm.user_key = t.user_key) left join travellog_entries te  "; 
  $sql.= "on t.key = te.travellog_key "; 
  $sql.= "group by tg.`name` "; 
  $sql.= "order by sum(te.est_miles) desc "; 
  

  $output .= "<table>";
  $output .= "<tr>";
  $output .= "<th>Group Name</th>";
  $output .= "<th>Total Participants</th>";
  $output .= "<th>Total Miles</th>";
  $output .= "</tr>";
  
  $result = db_query($sql);
  //get the results and build each table row
  //for each record returned
  $data = array();   
  $x_labels = array();
  while ($row = db_fetch_object($result)) {
    $output .= "<tr>";
    $output .= "<td>".$row->group_name."</td>";
    $output .= "<td>".$row->count_users."</td>";
    $output .= "<td>".$row->total_est_miles."</td>";
    $output .= "</tr>";
    $data[] = $row->total_est_miles;
    $x_labels[] = $row->group_name;
  }
  $output .= "</table>";
  
	$g = new open_flash_chart_api();
	$g->set_title( 'Total Mileage ' . date('Y'), '{font-size: 20px;}' );
	$g->set_width(600);
	
	//
	// BAR CHART:
	//
	$g->set_data( $data );
	$g->bar( 50, '#9933CC', '#8010A0', 'Miles', 10 );
	//
	// ------------------------
	//
	
	//
	// X axis tweeks:
	//
	$g->set_x_labels( $x_labels );
	//
	// set the X axis to show every 2nd label:
	//
	$g->set_x_label_style( 10, '#9933CC', 0, 1 );
	//
	// and tick every second value:
	//
	$g->set_x_axis_steps( 1 );
	//
	
	$g->set_y_max( max($data) );
	$g->set_y_label_steps( 4 );
	
	$g->set_y_legend( 'Miles', 12, '#736AFF' );
	$g->set_x_legend( 'Group', 12, '#736AFF' );
	
	$g->set_bg_colour('0xDFFFDF');
	
	$output .= "<br><br>";
	$output .= $g->render();
  return $output;
}//travellog_chart_group_summary

function travellog_chart_user_daily ($user_key) {
  if (!module_exists('open_flash_chart_api')) {
    return ' ';
  }
  
  global $user;
    
  $sql = "select m.month_desc, ifnull(a.total_est_miles,0) total_est_miles "; 
  $sql.= "from ( ";
  $sql.= "select 01 month_no, 'January' month_desc union ";
  $sql.= "select 02, 'February' union ";
  $sql.= "select 03, 'March' union ";
  $sql.= "select 04, 'April' union ";
  $sql.= "select 05, 'May' union ";
  $sql.= "select 06, 'June' union ";
  $sql.= "select 07, 'July' union ";
  $sql.= "select 08, 'August' union ";
  $sql.= "select 09, 'September' union ";
  $sql.= "select 10, 'October' union ";
  $sql.= "select 11, 'November' union ";
  $sql.= "select 12, 'December' ";
  $sql.= ") m left join ( ";
  $sql.= "select date_format(e.start_date, '%m') month_no, date_format(e.start_date, '%M') month_desc, sum(est_miles) total_est_miles "; 
  $sql.= "from travellog_entries e, travellog l  ";
  $sql.= "where l.key = e.travellog_key  ";
  $sql.= "and l.user_key = %d ";
  $sql.= "and date_format(e.start_date, '%Y') = date_format(curdate(), '%Y') ";
  $sql.= "group by date_format(e.start_date, '%M') ";
  $sql.= "order by date_format(start_date, '%Y%m') ";
  $sql.= ") a on m.month_no = a.month_no ";


  //query the database 
  $result = db_query($sql, $user->uid);
  
  //get the results and build each table row
  //for each record returned
  $data = array();   
  $x_labels = array();
  while ($row = db_fetch_object($result)) {
    $data[] = $row->total_est_miles;
    $x_labels[] = $row->month_desc;
  }
    
	$g = new open_flash_chart_api();
	$g->set_title( 'Monthly Mileage ' . date('Y'), '{font-size: 20px;}' );
	$g->set_width(600);
	
	//
	// BAR CHART:
	//
	$g->set_data( $data );
	$g->bar( 50, '#9933CC', '#8010A0', 'Miles', 10 );
	//
	// ------------------------
	//
	
	//
	// X axis tweeks:
	//
	$g->set_x_labels( $x_labels );
	//
	// set the X axis to show every 2nd label:
	//
	$g->set_x_label_style( 10, '#9933CC', 2, 1 );
	//
	// and tick every second value:
	//
	$g->set_x_axis_steps( 1 );
	//
	
	$g->set_y_max( max($data) );
	$g->set_y_label_steps( 4 );
	
	$g->set_y_legend( 'Miles', 12, '#736AFF' );
	$g->set_x_legend( 'Month', 12, '#736AFF' );
	
	$g->set_bg_colour('0xDFFFDF');
	return $g->render();
}

?>