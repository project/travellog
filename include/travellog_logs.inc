<?php
/**
* Generate HTML for all travel log entires of the current user
* @param travellog_key integer that filters out log entries based on parent travellog 
* @returns HTML page
*/
function travellog_logs_main ($travellog_key) {
 
  $output = '';
  $output .= "<table>";
  $output .= "<tr>";
  $output .= "<th>Log Entry Name</th>";
  $output .= "<th>Date</th>";
  $output .= "<th>Miles</th>";
  $output .= "<th>Duration</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "</tr>";
    $sql  = "select `key`, `name`, description, start_date, end_date, start_time, end_time, est_miles, est_time ";
    $sql .= "from travellog_entries ";
    $sql .= "where travellog_key = %d ";
    $sql .= "order by start_date, `key` ";
  
    $result = db_query($sql, $travellog_key);
  
    while ($row = db_fetch_object($result)) {
      $output .= "<tr>";
      $output .= "<td>".t($row->name)."</td>";
      $output .= "<td>".t($row->start_date)."</td>";
      $output .= "<td>".$row->est_miles."</td>";
      $output .= "<td>".$row->est_time."</td>";
      
      $output .= "<td><a href=\"" . $base_url . "/travellog/logs/" . $travellog_key . "/edit/" . $row->key .  "\">Edit</a></td>";
      $output .= "<td><a href=\"" . $base_url . "/travellog/logs/" . $travellog_key . "/delete/" . $row->key .  "\">Delete</a></td>";
      $output .= "</tr>";
    }
  $output .= "</table>";
  
  $output .= "<a href=\"" . $base_url . "/travellog/logs/" . $travellog_key . "/add\">>>Add Log Entry</a>&nbsp;&nbsp;";
  $output .= "<p></p>";
  return $output;
  
} //function travellog_logs_main 

/**
* Display the travel log add new, edit, and delete forms
* @param form_state by reference array of all post values of the form
* @param trans_type string that tells what type of transaction is being performed - add, edit, delete
* @param travellog_key integer that identifies the current travel log parent for listing all log entries
* @param travellog_logs_key integer that identifies the current record to edit or delete. null or 0 when adding a record 
* @return array form 
*/
function travellog_logs_form (&$form_state, $trans_type, $travellog_key, $travellog_logs_key){

  $debug_flag = FALSE;
  if ($debug_flag) {
    $debug = "trans_type= " . $trans_type . "<br>";
    $debug .= "travellog_key= " . $travellog_key . "<br>";
    $debug .= "travellog_logs_key= " . $travellog_logs_key  . "<br>";
   
    $form['debug'] = array(
      '#value' => t('<p>'. $debug . "</p>" ),
    );
  } 
  if ($trans_type == 'add') {
    $form_state['current_form'] = 'travellog_logs_add';
    $form['travellog_key'] = array('#type' => 'hidden', '#value' => $travellog_key );
    $form['title'] = array(
      '#value' => t('<p>Add Log Entry</p>' ),
    );
    
    $form['name'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => t('Name'),
      '#size' => 16,
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
    );
    $form['start_date'] = array(
      '#type' => 'date',
      '#title' => t('Start Date'),
    );
    $form['est_miles'] = array(
      '#type' => 'textfield',
      '#title' => t('Miles'),
      '#default_value' => '0',
      '#size' => 10,
    );
    $form['est_time'] = array(
      '#type' => 'textfield',
      '#title' => t('Travel Time (HH:MI)'),
      '#default_value' => '00:00',
      '#size' => 10,
    );
    $form['submit'] = array (
       '#type' => 'submit',
       '#value' => t('Save'),
    );
    $form['cancel'] = array (
       '#type' => 'button',
       '#value' => t('Cancel'),
       '#executes_submit_callback' => TRUE,
    );
  } //add
  else if ($trans_type == 'edit') {
    $form_state['current_form'] = 'travellog_logs_edit';
    $form['key']           = array('#type' => 'hidden', '#value' => $travellog_logs_key );
    $form['travellog_key'] = array('#type' => 'hidden', '#value' => $travellog_key );
    
    $sql  = "select `name`, description, start_date, start_time, end_date, end_time, est_miles, est_time ";
    $sql .= "from travellog_entries ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_logs_key );
    $row = db_fetch_object($result);

    $form['title'] = array(
      '#value' => t('<p>Edit ' . t($row->name) . " Log Entry </p>" ),
    );
    
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#size' => 16,
      '#default_value' => t($row->name),
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => t($row->description),
    );
    $date = explode ('-', $row->start_date);
    $form['start_date'] = array(
      '#type' => 'date',
      '#title' => t('Start Date'),
      '#default_value' => array('year' => $date[0], 'month' => intval($date[1]), 'day' => intval($date[2])),
    );
    $form['est_miles'] = array(
      '#type' => 'textfield',
      '#title' => t('Miles'),
      '#default_value' => '0',
      '#default_value' => $row->est_miles,
      '#size' => 10,
    );
    unset ($time);
    $time = explode (':', $row->est_time);
    $form['est_time'] = array(
      '#type' => 'textfield',
      '#title' => t('Travel Time'),
      '#default_value' => $time[0] . ':' . $time[1],
      '#size' => 10,
    );
	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Save'),
	 );
    $form['delete'] = array (
      '#type' => 'button',
	   '#value' => t('Delete'),
	   '#executes_submit_callback' => TRUE,
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 ); 
  } //edit 
  else if ($trans_type == 'delete') {
    $form_state['current_form'] = 'travellog_logs_delete';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_logs_key );
    $form['travellog_key'] = array('#type' => 'hidden', '#value' => $travellog_key );
    $sql  = "select `name`, description, start_date, start_time, end_date, end_time, est_miles, est_time ";
    $sql .= "from travellog_entries ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_logs_key );
    $row = db_fetch_object($result);

    $form['title'] = array(
      '#value' => t('<p>Delete ' . t($row->name) . " Log Entry </p>" ),
    );

	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Delete'),
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	   '#value' => t('Cancel'),
	   '#executes_submit_callback' => TRUE,
	 );
  } //delete 
  else {
    $form_state['current_form'] = 'travellog_logs_main';
    
    $form['logs_main'] = array(
      '#value' => t(travellog_logs_main($travellog_key)),
    );
    $form['cancel'] = array (
       '#type' => 'button',
       '#value' => t('Cancel'),
       '#executes_submit_callback' => TRUE,
    );
  } //anything else 
  
  return $form;
} //travellog_logs_form

/**
* Processes the form submit hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_logs_form_submit($form, &$form_state){
  global $user;
  global $base_url;

  if ($form_state['values']['op'] == 'Save') {
    if ($form_state['current_form'] == 'travellog_logs_add') {
       $est_time = explode(':', $form_state['values']['est_time']);
       $start_time = explode(':', $form_state['values']['start_time']);
       $end_time = explode(':', $form_state['values']['end_time']);
	    try {
	      $sql = "insert into travellog_entries "; 
	      $sql.= "(name, description, ";
	      $sql.= "travellog_key, start_date, end_date, ";
	      $sql.= "start_time, end_time, est_miles, est_time) ";
	      $sql.= "values ";
	      $sql.="('%s','%s',%d, '%s', '%s', '%s', '%s' , %d, '%s') ";
	      
	      if (!db_query ($sql
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['description'])
	                   , $form_state['values']['travellog_key']
	                   , $form_state['values']['start_date']['year'] . '-' . $form_state['values']['start_date']['month'] . '-' . $form_state['values']['start_date']['day'] 
	                   , $form_state['values']['end_date']['year'] . '-' . $form_state['values']['end_date']['month'] . '-' . $form_state['values']['end_date']['day']
	                   , $start_time[0] . ':' . $start_time[1]
	                   , $end_time[0] . ':' . $end_time[1]
	                   , $form_state['values']['est_miles'] 
	                   , $est_time[0] . ':' . $est_time[1]
	                   ) 
	         ) {
	         throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/logs/" . $form_state['values']['travellog_key'];
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //Add
    else if ($form_state['current_form'] == 'travellog_logs_edit') {
       $est_time = explode(':', $form_state['values']['est_time']);
       $start_time = explode(':', $form_state['values']['start_time']);
       $end_time = explode(':', $form_state['values']['end_time']);
	    try {
	      $sql = "update travellog_entries "; 
	      $sql.= "set ";
	      $sql.= "name = '%s', ";
	      $sql.= "description = '%s', ";
	      $sql.= "start_date = '%s', ";
	      $sql.= "end_date = '%s', ";
	      $sql.= "start_time = '%s', "; 
	      $sql.= "end_time = '%s', "; 
	      $sql.= "est_miles = %d, "; 
	      $sql.= "est_time = '%s' ";
         $sql.= "where `key` = %d ";
	      
	      if (!db_query ($sql
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['description'])
	                   , $form_state['values']['start_date']['year'] . '-' . $form_state['values']['start_date']['month'] . '-' . $form_state['values']['start_date']['day']
	                   , $form_state['values']['end_date']['year'] . '-' . $form_state['values']['end_date']['month'] . '-' . $form_state['values']['end_date']['day']
	                   , $start_time[0] . ':' . $start_time[1]
	                   , $end_time[0] . ':' . $end_time[1]
	                   , $form_state['values']['est_miles']
	                   , $est_time[0] . ':' . $est_time[1]
	                   , $form_state['values']['key']
	                   )
	          ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/logs/" . $form_state['values']['travellog_key'];
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //edit 
  } //Save
  else if ($form_state['values']['op'] == 'Delete') {
    if ($form_state['current_form'] == 'travellog_logs_edit') {
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = $base_url . "/travellog/logs/delete/".$form_state['values']['key'];
    } //travellog_vehicles_delete 
    else {
	    try {
	      $sql = "delete from travellog_entries "; 
	      $sql.= "where `key` = %d ";
	      	      
	      if (!db_query ($sql
	                   , $form_state['values']['key']
	                   )
	          ) {
	        throw new Exception('Could not delete record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been deleted.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/logs/" . $form_state['values']['travellog_key'];
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
	  } //travellog_delete 
  }//Delete
  else if ($form_state['values']['op'] == 'Cancel') {
    drupal_set_message(t('Cancelled.'),'status');
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = $base_url . "/travellog/logs/" . $form_state['values']['travellog_key'];
  } //cancel
} //travellog_logs_form_submit

/**
* Processes the form submit hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_logs_form_validate($from, &$form_state) {

  //don't validate if it's a cancel and not a delete 
  if ($form_state['values']['op'] == 'Save'){
    $name = trim(check_plain($form_state['values']['name']));
    if ($name=='') {
      form_set_error('Validation', t('Please enter a name'));
    }
	  if(!preg_match("/^[0-9]{2}\:[0-9]{2}$/", $form_state['values']['est_time'])) {
	    form_set_error('', t('Estimated Travel Time is in HH:MI'));
	  } 
	  unset($time);
	  $time = explode (':', $form_state['values']['est_time']);
	  if (intval ($time[0]) > 99) {
	    form_set_error('Validation', t('Est Travel Time Hour should be less than 99'));
	  }
	  if (intval($time[1]) > 59) {
	    form_set_error('Validation', t('Est Travel Time Minutes should be less than 60'));
	  }  
	}
} //travelllog_logs_form_validate 
?>