<?php
/**
* Generate HTML for all travel log vehicles of the current user
* @returns HTML page
*/
function travellog_vehicles_main () {
  global $user;
  global $base_url;
  $output = '';
  
  $output .= "<table>";
  $output .= "<tr>";
  $output .= "<th>Name</th>";
  $output .= "<th>Type</th>";
  $output .= "<th>Brand</th>";
  $output .= "<th>Model</th>";
  $output .= "<th>Year</th>";
  $output .= "<th>Est MPG (Cars)</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "</tr>";

  $sql  = "select v.`key`, v.travellog_vehicle_types_key, ";
  $sql .= "v.user_key, v.name vehicle_name, vt.name vehicle_type_name , brand, model, `year`, ";
  $sql .= "est_mpg_city, est_mpg_hwy, primary_flag ";
  $sql .= "from travellog_vehicles v left join travellog_vehicle_types vt ";
  $sql .= "on v.travellog_vehicle_types_key = vt.key "; 
  $sql .= "where user_key = %d ";
  $sql .= "order by v.`key` ";
  
  $result = db_query($sql, $user->uid);
  
  while ($row = db_fetch_object($result)) {
    $output .= "<tr>";
    $output .= "<td>".t($row->vehicle_name)."</td>";
    $output .= "<td>".t($row->vehicle_type_name)."</td>";
    $output .= "<td>".t($row->brand)."</td>";
    $output .= "<td>".t($row->model)."</td>";
    $output .= "<td>".$row->year ."</td>";
    $output .= "<td>".$row->est_mpg_city."</td>";
    $output .= "<td><a href=\"" . $base_url . "/travellog/vehicles/edit/" . $row->key . "\">Edit</a></td>";
    $output .= "<td><a href=\"" . $base_url . "/travellog/vehicles/delete/" . $row->key . "\">Delete</a></td>";
    $output .= "</tr>";

  }
  $output .= "</table>";  
  $output .= "<a href=\"" . $base_url . "/travellog/vehicles/add\">>>Add Ride</a>&nbsp;&nbsp;";
  $output .= "<p></p>";
  return $output;  
} //function travellog_vehicles_main 

/**
* Display the travel log vehicle add new, edit, and delete forms
* @param form_state by reference array of all post values of the form
* @param trans_type string that tells what type of transaction is being performed - add, edit, delete
* @param travellog_vehicles_key integer that identifies the current record to edit or delete. null or 0 when adding a record 
* @return array form 
*/
function travellog_vehicles_form (&$form_state, $trans_type, $travellog_vehicles_key){
  if ($trans_type == 'add') {
    $form_state['current_form'] = 'travellog_vehicles_add';
    
    $options[] = t('<select type>');
    $sql = 'select `key`, name from travellog_vehicle_types order by 2';
    $result = db_query ($sql);
	  while ($row = db_fetch_object($result)) {
	    $options[$row->key] = t($row->name);
    }
	  $form['travellog_vehicle_types_key'] = array(
	    '#type' => 'select',
	    '#title' => t('Vehicle'),
	    '#default_value' => 0,
	    '#options' => $options, 
	  );

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Nickname'),
    );
    	 
    $form['brand'] = array(
      '#type' => 'textfield',
      '#title' => t('Brand'),
    );
    $form['model'] = array(
      '#type' => 'textfield',
      '#title' => t('Model'),
    );
    $form['year'] = array(
      '#type' => 'textfield',
      '#title' => t('Year'),
      '#size' => 4,
    );
    $form['est_mpg_city'] = array(
      '#type' => 'textfield',
      '#title' => t('Est MPG'),
      '#size' => 10,
    );
    $form['submit'] = array (
      '#type' => 'submit',
      '#value' => t('Save'),
	    '#executes_submit_callback' => TRUE,
    );
    $form['cancel'] = array (
      '#type' => 'button',
      '#value' => t('Cancel'),
	    '#executes_submit_callback' => TRUE,
    );

  } //add
  else if ($trans_type == 'edit') {
    $form_state['current_form'] = 'travellog_vehicles_edit';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_vehicles_key );

    $sql  = "select travellog_vehicle_types_key, user_key, name, brand, model, `year`, est_mpg_city, est_mpg_hwy, primary_flag ";
    $sql .= "from travellog_vehicles ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_vehicles_key );
    $vehicles_row = db_fetch_object($result);
    
    $form['title'] = array(
      '#value' => t('<p>Edit Vehicle '. t($vehicles_row->name) . "</p>" ),
    );
        
    $options[] = t('<select type>');
    $sql = 'select `key`, name from travellog_vehicle_types order by 2';
    $result = db_query ($sql);
	 while ($row = db_fetch_object($result)) {
	   $options[$row->key] = t($row->name);
    }

	 $form['travellog_vehicle_types_key'] = array(
	   '#type' => 'select',
	   '#title' => t('Vehicle'),
	   '#default_value' => $vehicles_row->travellog_vehicle_types_key,
	   '#options' => $options,
	 );

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Nickname'),
      '#default_value' => t($vehicles_row->name),
    );
    	 
    $form['brand'] = array(
      '#type' => 'textfield',
      '#title' => t('Brand'),
      '#default_value' => t($vehicles_row->brand),
    );
    $form['model'] = array(
      '#type' => 'textfield',
      '#title' => t('Model'),
      '#default_value' => t($vehicles_row->model), 
    );
    $form['year'] = array(
      '#type' => 'textfield',
      '#title' => t('Year'),
      '#size' => 4,
      '#default_value' => $vehicles_row->year, 
    );
    $form['est_mpg_city'] = array(
      '#type' => 'textfield',
      '#title' => t('Est MPG'),
      '#size' => 10,
      '#default_value' => $vehicles_row->est_mpg_city, 
    );

	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Save'),
	 );
    $form['delete'] = array (
      '#type' => 'button',
	    '#value' => t('Delete'),
	    '#executes_submit_callback' => TRUE,
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	    '#value' => t('Cancel'),
	    '#executes_submit_callback' => TRUE,
	 ); 
  } //edit 
  else if ($trans_type == 'delete') {
    $form_state['current_form'] = 'travellog_vehicles_delete';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_vehicles_key );
    
    $sql  = "select travellog_vehicle_types_key, user_key, name, brand, model, `year`, est_mpg_city, est_mpg_hwy, primary_flag ";
    $sql .= "from travellog_vehicles ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_vehicles_key );
    $row = db_fetch_object($result);
    
    $form['title'] = array(
      '#value' => t('<p>Delete Vehicle '. t($row->name) . "</p>" ),
    );
    
	 $form['submit'] = array (
	   '#type' => 'submit',
	   '#value' => t('Delete'),
	 );
    $form['cancel'] = array (
      '#type' => 'button',
	    '#value' => t('Cancel'),
	    '#executes_submit_callback' => TRUE,
	 );
  } //delete 
  else {
    $form_state['current_form'] = 'travellog_vehicles_main';
    $form['logs_main'] = array(
      '#value' => t(travellog_vehicles_main()),
    );
  } //anything else
  
  return $form;
}//travellog_vehicles_form


/**
* Processes the form validate hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_vehicles_form_validate($form, &$form_state) {
  //don't validate if it's a cancel
  if ($form_state['values']['op'] == 'Save'){
    $name = trim(check_plain($form_state['values']['name']));
    if ($name=='') {
      form_set_error('Validation', t('Please name your ride'));
    }
    if ($form_state['values']['travellog_vehicle_types_key'] == 0) {
      form_set_error('Validation', t('Please select the type of ride'));
    }
  }
} //function travellog_vehicles_form_validate 

/**
* Processes the form submit hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_vehicles_form_submit ($form, &$form_state) {
  global $user;
  global $base_url;
  
  //drupal_set_message(t('Test'),'status');
  if ($form_state['values']['op'] == 'Save') {
    if ($form_state['current_form'] == 'travellog_vehicles_add') {
	    try {
	      $sql = "insert into travellog_vehicles "; 
	      $sql.= "(travellog_vehicle_types_key, user_key, name, brand, model, year, est_mpg_city, est_mpg_hwy, primary_flag) ";
	      $sql.= "values ";
	      $sql.="(%d,%d,'%s','%s','%s',%d,%d,%d,%d) ";
	      
	      if (!db_query ($sql
	                   , $form_state['values']['travellog_vehicle_types_key']
	                   , $user->uid 
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['brand'])
	                   , trim($form_state['values']['model']) 
	                   , $form_state['values']['year']
	                   , $form_state['values']['est_mpg_city']
	                   , null
	                   , 0 
	                   )
	          ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/vehicles";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //Add
    else if ($form_state['current_form'] == 'travellog_vehicles_edit') {
	    try {
	      $sql = "update travellog_vehicles "; 
	      $sql.= "set ";
	      $sql.= "travellog_vehicle_types_key = %d, ";
	      $sql.= "user_key = %d, ";
	      $sql.= "name = '%s', ";
	      $sql.= "brand = '%s',  ";
	      $sql.= "model = '%s',  ";
	      $sql.= "year = %d, ";
	      $sql.= "est_mpg_city = %d, ";
	      $sql.= "est_mpg_hwy = %d, ";
	      $sql.= "primary_flag = %d ";
         $sql.= "where `key` = %d ";
	      
	      if (!db_query ($sql
	                   , $form_state['values']['travellog_vehicle_types_key']
	                   , $user->uid 
		                 , trim($form_state['values']['name'])
	                   , trim($form_state['values']['brand'])
	                   , trim($form_state['values']['model'])
	                   , $form_state['values']['year']
	                   , $form_state['values']['est_mpg_city']
	                   , null //est_mpg_hwy 
	                   , 0   //primary_flag
	                   , $form_state['values']['key'] 
	                   )
	          ) {
	        throw new Exception('Could not update record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been saved.'),'status');
	      $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/vehicles";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
    } //edit 
  } //Save
  else if ($form_state['values']['op'] == 'Delete') {
    if ($form_state['current_form'] == 'travellog_vehicles_edit') {
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = $base_url . "/travellog/vehicles/delete/".$form_state['values']['key'];
    } //travellog_vehicles_delete 
    else {
	    try {
	      $sql = "delete from travellog_vehicles "; 
	      $sql.= "where `key` = %d ";
	      	      
	      if (!db_query ($sql
	                   , $form_state['values']['key']
	                   )
	          ) {
	        throw new Exception('Could not delete record!');
	      }
	      //set the status
	    	      drupal_set_message(t('Your record has been deleted.'),'status');
	      $form_state['rebuild'] = FALSE;
        $form_state['redirect'] = $base_url . "/travellog/vehicles";
	    } //try
	    catch (Exception $e) {
	      //set the error
		   drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');	    } //catch
	  } //travellog_vehicles_delete 
  }//Delete
  else if ($form_state['values']['op'] == 'Cancel') {
    drupal_set_message(t('Cancelled.'),'status');
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = $base_url . "/travellog/vehicles";
  } //cancel
} //vehicles_form_submit 
?>