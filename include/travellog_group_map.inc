<?php

/**
* Generate HTML for all travel log group map 
* @returns HTML page
*/
function travellog_group_map_main () {
  global $user;
  $output = '';
  $output .= "<table>";
  $output .= "<tr>";
  $output .= "<th>Group Name</th>";
  $output .= "<th>User Name</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "<th>&nbsp;</th>";
  $output .= "</tr>";

  $sql  = "select gm.`key`, gm.user_key, u.`name` as user_name, gm.travellog_groups_key, g.`name` as group_name ";
  $sql .= "from (travellog_group_map gm join users u on gm.user_key = u.uid) join travellog_groups g on gm.travellog_groups_key = g.key ";
  $sql .= "order by 5,3 "; 
  
  $result = db_query($sql, $user->uid);
  
  while ($row = db_fetch_object($result)) {
    $output .= "<tr>";
    $output .= "<td>".$row->group_name."</td>";
    $output .= "<td>".$row->user_name ."</td>";
    $output .= "<td><a href=\"" . $base_url . "/travellog/group_map/edit/" . $row->key . "\">Edit</a></td>";
    $output .= "<td><a href=\"" . $base_url . "/travellog/group_map/delete/" . $row->key . "\">Delete</a></td>";
    $output .= "</tr>";
  }

  $output .= "</table>";
  
  $output .= "<a href=\"" . $base_url . "/travellog/group_map/add\">>>Add</a>&nbsp;&nbsp;";
  $output .= "<p></p>";
  return $output;
} //function travellog_group_map_form 

/**
* Display the travel log group map add new, edit, and delete forms
* @param form_state by reference array of all post values of the form
* @param trans_gruop string that tells what type of transaction is being performed - add, edit, delete
* @param travellog_group_map_key integer that identifies the current record to edit or delete. null or 0 when adding a record 
* @return array form 
*/
function travellog_group_map_form (&$form_state, $trans_type, $travellog_group_map_key){

  if ($trans_type == 'add') {
    $form_state['current_form'] = 'travellog_group_map_add';
    
	 unset($options);
    $options[0] = t('<select user>');
    $sql = "select `uid`, `name` from {users} where uid > 1 order by 2 ";
    $result = db_query ($sql);
	  while ($row = db_fetch_object($result)) {
	    $options[$row->uid] = t($row->name);
    }
	  $form['user_key'] = array(
	    '#type' => 'select',
	    '#title' => t('User'),
	    '#default_value' => 0,
	    '#options' => $options, 
	  );
	  
    unset($options);
    $options[0] = t('<select group>');
    $sql = 'select `key`, name from travellog_groups order by 2';
    $result = db_query ($sql);
	  while ($row = db_fetch_object($result)) {
	    $options[$row->key] = t($row->name);
    }
	  $form['travellog_groups_key'] = array(
	    '#type' => 'select',
	    '#title' => t('Group'),
	    '#default_value' => 0,
	    '#options' => $options, 
	  );
	 
  
    $form['submit'] = array (
       '#type' => 'submit',
       '#value' => t('Save'),
    );
    $form['cancel'] = array (
       '#type' => 'button',
       '#value' => t('Cancel'),
       '#executes_submit_callback' => TRUE,
    );

  } //add
  else if ($trans_type == 'edit') {
    $form_state['current_form'] = 'travellog_group_map_edit';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_group_map_key );
    
    $sql  = "select `key`, travellog_groups_key, user_key ";
    $sql .= "from travellog_group_map ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_group_map_key );
    $group_row = db_fetch_object($result);
    
    $options[] = t('<select group>');
    $sql = 'select `key`, name from travellog_groups order by 2';
    $result = db_query ($sql);
	  while ($row = db_fetch_object($result)) {
	    $options[$row->key] = t($row->name);
    }
	  $form['travellog_groups_key'] = array(
	    '#type' => 'select',
	    '#title' => t('Group'),
	    '#default_value' => $group_row->travellog_groups_key,  
	    '#options' => $options, 
	  );
	  
	 unset($options);
    $options[] = t('<select user>');
    $sql = 'select uid, `name` from users order by 2';
    $result = db_query ($sql);
	  while ($row = db_fetch_object($result)) {
	    $options[$row->uid] = t($row->name);
    }
	  $form['user_key'] = array(
	    '#type' => 'select',
	    '#title' => t('User'),
	    '#default_value' => $group_row->user_key,
	    '#options' => $options, 
	  );

   $form['submit'] = array (
     '#type' => 'submit',
     '#value' => t('Save'),
   );
    $form['delete'] = array (
      '#type' => 'button',
      '#value' => t('Delete'),
      '#executes_submit_callback' => TRUE,
   );
    $form['cancel'] = array (
      '#type' => 'button',
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
   ); 
  } //edit 
  else if ($trans_type == 'delete') {
    $form_state['current_form'] = 'travellog_group_map_delete';
    $form['key'] = array('#type' => 'hidden', '#value' => $travellog_group_map_key );
    
    $sql  = "select `key`, user_key, travellog_groups_key ";
    $sql .= "from travellog_group_map ";
    $sql .= "where `key` = %d ";
  
    $result = db_query($sql, $travellog_group_map_key);
    $row = db_fetch_object($result);
       
    $form['title'] = array(
      '#value' => t("<p>Delete User Group Record?</p>" ),
    );
    $form['submit'] = array (
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
    $form['cancel'] = array (
      '#type' => 'button',
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
    );
  } //delete 
  else {
    $form_state['current_form'] = 'travellog_group_map_main';
    $form['title'] = array(
      '#value' => t(travellog_group_map_main()),
    );
  } //anything else
  return $form;
} //function travellog_group_map_form 

/**
* Processes the form validate hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_group_map_form_validate($form, &$form_state) {
  //don't validate if it's a cancel
  if ($form_state['values']['op'] == 'Save'){
    if ($form_state['values']['user_key'] == 0) {
      form_set_error('Validation', t('Pleas select a user'));
    }
    if ($form_state['values']['travellog_groups_key'] == 0) {
      form_set_error('Validation', t('Pleas select a group'));
    }
  }
} //function travellog_group_map_form_validate 
/**
* Processes the form submit hook
* @param form array of the current form to be processed 
* @param form_state by reference array of all post values of the form
* @return array form 
*/
function travellog_group_map_form_submit ($form, &$form_state) {
  global $user;
  global $base_url;

  if ($form_state['values']['op'] == 'Save') {
    if ($form_state['current_form'] == 'travellog_group_map_add') {
      try {
        $sql = "insert into travellog_group_map "; 
        $sql.= "(user_key, travellog_groups_key) ";
        $sql.= "values ";
        $sql.="(%s,%d) ";
        
        if (!db_query ($sql 
                    , trim($form_state['values']['user_key'])
                    , trim($form_state['values']['travellog_groups_key'])
                     )
            ) {
          throw new Exception('Could not update record!');
        }
        //set the status
              drupal_set_message(t('Your record has been saved.'),'status');
        $form_state['rebuild'] = FALSE;
        $form_state['redirect'] = $base_url . "/travellog/group_map";
      } //try
      catch (Exception $e) {
        //set the error
       drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');      } //catch
    } //Add
    else if ($form_state['current_form'] == 'travellog_group_map_edit') {
      try {
        $sql = "update travellog_group_map "; 
        $sql.= "set ";
        $sql.= "user_key = %d, ";
        $sql.= "travellog_groups_key = %d ";
        $sql.= "where `key` = %d ";
        
        if (!db_query ($sql
                    , trim($form_state['values']['user_key'])
                    , trim($form_state['values']['travellog_groups_key'])
                    , $form_state['values']['key']
                    )
            ) {
          throw new Exception('Could not update record!');
        }
        //set the status
              drupal_set_message(t('Your record has been saved.'),'status');
        $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/group_map";
      } //try
      catch (Exception $e) {
        //set the error
       drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');      } //catch
    } //edit 
  } //Save
  else if ($form_state['values']['op'] == 'Delete') {
    if ($form_state['current_form'] == 'travellog_group_map_edit') {
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = $base_url . "/travellog/group_map/delete/".$form_state['values']['key'];
    } //travellog_group_map_delete 
    else {
      try {
        $sql = "delete from travellog_group_map "; 
        $sql.= "where `key` = %d ";
                
        if (!db_query ($sql
                     , $form_state['values']['key']
                     )
            ) {
          throw new Exception('Could not delete record!');
        }
        //set the status
              drupal_set_message(t('Your record has been deleted.'),'status');
        $form_state['rebuild'] = FALSE;
         $form_state['redirect'] = $base_url . "/travellog/group_map";
      } //try
      catch (Exception $e) {
        //set the error
       drupal_set_message(t('Your new record could not be saved.  There was an error.'),'error');      } //catch
    } //travellog_group_map_delete 
  }//Delete
  else if ($form_state['values']['op'] == 'Cancel') {
    drupal_set_message(t('Cancelled.'),'status');
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = $base_url . "/travellog/group_map";
  } //cancel
} //function travellog_group_map_form_submit
?>